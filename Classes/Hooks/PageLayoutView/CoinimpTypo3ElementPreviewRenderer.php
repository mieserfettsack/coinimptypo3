<?php

namespace Mdy\Coinimptypo3\Hooks\PageLayoutView;

use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Contains a preview rendering for the page module of CType="yourextensionkey_newcontentelement"
 */
class CoinimpTypo3ElementPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "My new content element"
     *
     * @param PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    )
    {
        if ($row['CType'] === 'coinimp_typo3_element') {
            $this->pi_flexform = $this->convertFlexFormToArray($row['pi_flexform']);
            $itemContent .= '
                <p>'.LocalizationUtility::translate(
                    'LLL:EXT:coinimptypo3/Resources/Private/Language/locallang.xlf:be.preview.cpu.usage'
                ).': ' . $this->getPiFlexFormValueByIndex('settings.limit') . '</p>';
            $drawItem = false;
        }
    }

    /**
     * @return array
     */
    public $pi_flexform;

    /**
     * @param $data
     * @return array
     */
    public function convertFlexFormToArray($data)
    {
        $flexFormArray = GeneralUtility::xml2array($data);
        return $flexFormArray['data']['sDEF']['lDEF'];
    }

    /**
     * @param $index
     * @return array
     */
    public function getPiFlexFormValueByIndex($index)
    {
        $pi_flexform = $this->pi_flexform;
        return $pi_flexform[$index]['vDEF'];
    }
}
