Feature('Miner included as content element');

Scenario('Precache and test availability of test pages', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-content-element/default');
});

Scenario('Miner as content element included', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-content-element/default');
    I.see('Constants used on this page:\n' +
        'IncludeAsContentElement loaded', 'pre');
    I.see('Help us by clicking the logo! How you help us?', '#coinimptypo3__content__infotext__wrap');
    I.coinimpMinerisThrottleSet(0.5);
});

Scenario('Miner as content element included, user read info and start miner', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-content-element/default');
    I.dontSeeCheckboxIsChecked('#allowMintMeMining');
    I.waitForClickable('#coinimptypo3__content__infoclick');
    I.click('#coinimptypo3__content__infoclick');
    I.waitForVisible('#coinimptypo3__content__infotext__how');
    I.see('By clicking this logo your browser will start mining Mintme coins. If you enable autostart, your decision will be saved inside a cookie and you do not need to start it again.');
    I.click('#coinimptypo3__content__infotext__close');
    I.waitForInvisible('#coinimptypo3__content__infotext__how');
    I.click('.icon-content-element-coinimp-logo-without-borders');
    I.waitForVisible('#coinimptypo3__content__running__stats');
    I.see('The miner is currently running!');
    I.wait(2);
    I.coinimpMinerIsRunning();
    I.coinimpMinerisThrottleSet(0.5);
});

Scenario('Miner as content element included, enabled autostart, starts miner and reload page', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-content-element/default');
    I.dontSeeCheckboxIsChecked('#allowMintMeMining');
    I.checkOption('#allowMintMeMining');
    I.click('.icon-content-element-coinimp-logo-without-borders');
    I.waitForVisible('#coinimptypo3__content__running__stats');
    I.refreshPage();
    I.wait(2);
    I.coinimpMinerIsRunning();
    I.coinimpMinerisThrottleSet(0.5);
});
