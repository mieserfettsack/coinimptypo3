Feature('Background miner: default usage');

Scenario('Precache and test availability of test pages', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default');
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default-with-additional-info-text');
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/infobox-template-overwrite');
});

Scenario('Default background miner included', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default');
    I.see('Constants used on this page:\n' +
        '{$coinimptypo3.settings.used}', 'pre');
    I.see('We would like to create MINTME coins during your visit with your browser.', '.coinimptypo3__notification__p');
    I.dontSee('For more information about creating MINTME coins please click here.', '.coinimptypo3__notification__p');
    I.see('Create coins / No thanks (A cookie will be created to save your decision)', '.coinimptypo3__notification__p');
    I.startBackgroundMinerWithClick();
});

Scenario('Default background miner with extra info page about coinimp', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default-with-additional-info-text');
    I.see('Constants used on this page:\n' +
        'plugin.mdy.coinimptypo3.info.more.link.parameter = 1\n' +
        'plugin.mdy.coinimptypo3.info.more.link.text = clicki clicki here', 'pre');
    I.see('We would like to create MINTME coins during your visit with your browser. For more information about creating MINTME coins please clicki clicki here', '.coinimptypo3__notification__p');
    I.see('Create coins / No thanks (A cookie will be created to save your decision)', '.coinimptypo3__notification__p__decision');
    I.startBackgroundMinerWithClick();
});

Scenario('Default background miner and editor overwrites default template via TypoScript constant', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/infobox-template-overwrite');
    I.see('Overwrite template', 'h1');
    I.startBackgroundMinerAutomaticaly();
});

Scenario('Default background miner and user changes CPU usage in percent', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default');
    I.startBackgroundMinerWithClick();
    I.click('#coinimptypo3__running__box');
    I.waitForVisible('#coinimptypo3__running__box__config__throttle');
    I.fillField('#coinimptypo3__running__box__config__throttle', '100');
    I.pressKey('Enter');
    I.seeTextEquals('100', '#coinimptypo3__running__box__stats__throttle');
    I.fillField('#coinimptypo3__running__box__config__throttle', '50');
    I.pressKey('Enter');
    I.seeTextEquals('50', '#coinimptypo3__running__box__stats__throttle');
    I.fillField('#coinimptypo3__running__box__config__throttle', 'any non INT value');
    I.pressKey('Enter');
    I.fillField('#coinimptypo3__running__box__config__throttle', '0.1');
    I.pressKey('Enter');
    I.seeTextEquals('50', '#coinimptypo3__running__box__stats__throttle');
    I.fillField('#coinimptypo3__running__box__config__throttle', '-100000');
    I.pressKey('Enter');
    I.seeTextEquals('1', '#coinimptypo3__running__box__stats__throttle');
});

Scenario('Default background miner and user opens and closes infobox', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default');
    I.startBackgroundMinerWithClick();
    I.click('#coinimptypo3__running__box');
    I.waitForVisible('#coinimptypo3__running__box__close');
    I.waitForText('This website is currently');
    I.click('#coinimptypo3__running__box__close');
    I.waitForInvisible('#coinimptypo3__running__box__detail_wrap');
});

Scenario('Default background miner and user does not allow mining', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default');
    I.waitForVisible('.coinimptypo3__notification__a__forbid');
    I.click('.coinimptypo3__notification__a__forbid');
    I.waitForInvisible('#coinimptypo3__notification__box');
    I.refreshPage();
    I.waitForInvisible('#coinimptypo3__notification__box');
});

Scenario('Default background miner, user does allow mining and the miner starts automatically after reload', (I) => {
    I.amOnPage('http://coinimptypo3.ddev.site/include-as-background-miner/default');
    I.waitForVisible('.coinimptypo3__notification__a__allow');
    I.click('.coinimptypo3__notification__a__allow');
    I.refreshPage();
    I.dontSee('#coinimptypo3__running__box');
    I.dontSee('#coinimptypo3__notification__box');
});
