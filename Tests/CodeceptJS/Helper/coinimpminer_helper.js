const { Helper } = codeceptjs;
const assert = require('assert');

class CoinimpMiner extends Helper {
  async coinimpMinerStart() {
    const page = this.helpers['Puppeteer'].page;
    let start = await page.evaluate(function () {
      return coinimpMiner.start();
    });
  }

  async coinimpMinerIsRunning() {
    const page = this.helpers['Puppeteer'].page;
    let isRunning = await page.evaluate(function () {
      return coinimpMiner.isRunning();
    });

    if (isRunning === true) {
      assert.ok("coinimp miner is running");
    } else {
      assert.fail("coinimp miner is not running");
    }
  }

  async coinimpMinerisThrottleSet(value) {
    const page = this.helpers['Puppeteer'].page;
    let getThrottle = await page.evaluate(function () {
      return coinimpMiner.getThrottle();
    });

    if (getThrottle === value) {
      assert.ok("Throttle is correct");
    } else {
      assert.fail("Throttle is not correct. Current value is: " + getThrottle + " but should be " + value);
    }
  }
}

module.exports = CoinimpMiner;
