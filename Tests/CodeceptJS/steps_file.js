// in this file you can append custom step methods to 'I' object

module.exports = function() {
  return actor({
    checkIfCookiesAreSet: function(){
      I = this;

      I.seeCookie('allowMINTMECreation');
      I.seeCookie('coinimpThrottle');
    },

    setCookiesNeededForAutostart: function(throttleValue) {
      throttleValue = throttleValue || 0.9;
      I = this;

      I.setCookie([
        {name: 'allowMINTMECreation', value: "1"},
        {name: 'coinimpThrottle', value: "0.9"}
      ]);
    },

    startBackgroundMinerWithClick: function (throttleValue) {
      throttleValue = throttleValue || 0.9;
      I = this;

      I.waitForElement('.coinimptypo3__notification__a__allow');
      I.waitForFunction(() => window["coinimpMiner"]);
      I.click('.coinimptypo3__notification__a__allow');
      I.waitForVisible('#coinimptypo3__running__box');
      I.coinimpMinerIsRunning();
      I.coinimpMinerisThrottleSet(throttleValue);
      I.checkIfCookiesAreSet();
    },

    startBackgroundMinerAutomaticaly: function () {
      I = this;

      I.setCookiesNeededForAutostart(0.9);
      I.coinimpMinerIsRunning();
      I.coinimpMinerisThrottleSet(0.9);
      I.checkIfCookiesAreSet();
    }
  });
};
