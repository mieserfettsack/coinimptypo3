/**
 * @return {boolean}
 */
function IsDOMTokenListAvailable() {
    return (DOMTokenList && DOMTokenList != null);
}

if (IsDOMTokenListAvailable) {
    var
        totalHashes = 0,
        CPUUsage = 0,
        coinimpMinerCounter = 0,
        timeToRefreshInfo = 250,
        coinimpMiner,
        coinimpTypo3Settings,
        informAboutAVrunning = function () {
            alert("It looks like you are running some blocker or antivirus software. Please disable any tool to allow mining, thanks! :-*");
        },
        getObjectByName = function (object, string) {
            var explodedString = string.split('.');
            for (var i = 0, l = explodedString.length; i < l; i++) {
                object = object[explodedString[i]];
            }
            return object;
        },
        setObjectByName = function (object, keys, val) {
            keys = Array.isArray(keys) ? keys : keys.split('.');
            if (keys.length > 1) {
                object[keys[0]] = object[keys[0]] || {};
                return setObjectByName(object[keys[0]], keys.slice(1), val);
            }
            object[keys[0]] = val;
        },
        getCookie = function (name) {
            var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
            return v ? v[2] : null;
        },
        setCookie = function (name, value, days) {
            var d = new Date;
            d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
            document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
        },
        changeDisplay = function (selector, attribute) {
            if (document.querySelector(selector) && attribute) {
                document.querySelector(selector).style.display = attribute;
            }
        },
        toggleClass = function (selector, classname) {
            if (document.querySelector(selector) && classname) {
                document.querySelector(selector).classList.toggle(classname);
            }
        },
        doNotAllowMINTMECreation = function () { // eslint-disable-line no-unused-vars
            setCookie("allowMINTMECreation", 0, 365);
            changeDisplay("#coinimptypo3__notification__box", "none");
        },
        allowMINTMECreation = function () { // eslint-disable-line no-unused-vars
            if (typeof coinimpMiner !== "undefined") {
                setCookie("allowMINTMECreation", 1, 365);
                setCookie("coinimpThrottle", getObjectByName(coinimpTypo3Settings, "options.throttle"), 365);
                changeDisplay("#coinimptypo3__notification__box", "none");
                startCoinimpMiner();
            } else {
                informAboutAVrunning();
            }
        },
        showIsRunningBox = function () {
            changeDisplay("#coinimptypo3__running__box", "block");
        },
        executeCoinimpMiner = function (functionName, functionValue) {
            if (functionName) {
                if (typeof coinimpMiner !== "undefined") {
                    return coinimpMiner[functionName](functionValue);
                } else {
                    informAboutAVrunning();
                }
            }
        },
        startCoinimpMiner = function () {
            executeCoinimpMiner('start');
            showIsRunningBox();
        },
        appendJavaScriptFileToHead = function (url, callback) {
            var script = document.createElement("script");
            script.type = "text/javascript";

            if (script.readyState) {  //IE
                script.onreadystatechange = function () {
                    if (script.readyState === "loaded" ||
                        script.readyState === "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else {
                script.onload = function () {
                    callback();
                };
            }

            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        },
        getCoinimpSettingsFromTYPO3 = function (file, callback) {
            var getJSONFile = new XMLHttpRequest();
            getJSONFile.overrideMimeType("application/json");
            getJSONFile.open("GET", file, true);
            getJSONFile.onreadystatechange = function () {
                if (getJSONFile.readyState === 4 && getJSONFile.status === 200) {
                    callback(getJSONFile.responseText);
                }
            };
            getJSONFile.send(null);
        },
        castAllToInt = function (string) {
            return isNaN(parseInt(string, 10)) ? 0 : parseInt(string, 10);
        },
        populateConfig = function () {
            if (getCookie("coinimpThrottle")) {
                document.querySelector(
                    "#coinimptypo3__running__box__config__throttle").value = 100 - (getCookie("coinimpThrottle") * 100
                );
            }
        },
        changeThrottleHandler = function () {
            var configThrottle = document.querySelector("#coinimptypo3__running__box__config__throttle");

            if (getCookie("coinimpThrottle")) {
                configThrottle.value = (100 - castAllToInt(getCookie("coinimpThrottle"))) / 100;
            }

            function updateThrottleValue() {
                var
                    setThrottleValue = 0.0,
                    configThrottleValue = castAllToInt(configThrottle.value);

                if (configThrottleValue === 0) {
                    setThrottleValue = 0.5;
                } else if (configThrottleValue > 100) {
                    setThrottleValue = 0.0;
                } else {
                    setThrottleValue = (100 - configThrottleValue) / 100;
                }

                executeCoinimpMiner('setThrottle', setThrottleValue);
                setCookie("coinimpThrottle", setThrottleValue, 365);
            }

            configThrottle.addEventListener('keypress', updateThrottleValue);
            configThrottle.addEventListener('keyup', updateThrottleValue);
            configThrottle.addEventListener('input', updateThrottleValue);
            configThrottle.addEventListener('change', updateThrottleValue);
        },
        isMinerLoaded = setInterval(function () {
            if (typeof coinimpMiner === "object" && getCookie("allowMINTMECreation") === "1") {
                startCoinimpMiner();
                if (document.querySelector('#coinimptypo3__running__box')) {
                    changeThrottleHandler();
                    populateConfig();
                }
                clearInterval(isMinerLoaded);
            }

            if (getCookie("allowMINTMECreation") === "0") {
                clearInterval(isMinerLoaded);
            }

            if (coinimpMinerCounter > 30) {
                clearInterval(isMinerLoaded);
            }

            coinimpMinerCounter++;
        }, 100),
        showHasPerSeconds = setInterval(function () { // eslint-disable-line no-unused-vars
            var hashPerSeconds;
            if (
                document.querySelector('#coinimptypo3__running__box') ||
                document.querySelector('#coinimptypo3__content__running__stats__hashes')
            ) {
                if (typeof coinimpMiner === "object") {
                    hashPerSeconds = executeCoinimpMiner('getHashesPerSecond');
                    if (getCookie("allowMINTMECreation") === "1") {
                        var elemHashPerSecond = document.querySelector(
                            '#coinimptypo3__running__box__stats__hashPerSecond'
                        );
                        if (elemHashPerSecond) {
                            elemHashPerSecond.innerHTML = Math.round(hashPerSeconds);
                        }
                    }

                    if (document.querySelector('#coinimptypo3__content__running__stats__hashes')) {
                        var elemHashPerSecondContent = document.querySelector(
                            '#coinimptypo3__content__running__stats__hashes'
                        );
                        if (elemHashPerSecondContent) {
                            elemHashPerSecondContent.innerHTML = Math.round(hashPerSeconds);
                        }
                    }
                }
            } else {
                clearInterval(showHasPerSeconds);
            }
        }, timeToRefreshInfo),
        showTotalHashes = setInterval(function () { // eslint-disable-line no-unused-vars
            if (document.querySelector('#coinimptypo3__running__box')) {
                if (typeof coinimpMiner === "object" && getCookie("allowMINTMECreation") === "1") {
                    totalHashes = executeCoinimpMiner('getTotalHashes', true);
                    var elemTotalHashes = document.querySelector('#coinimptypo3__running__box__stats__totalHashes');
                    elemTotalHashes.innerHTML = totalHashes;
                }
            } else {
                clearInterval(showTotalHashes);
            }
        }, timeToRefreshInfo),
        showCPUUsage = setInterval(function () {// eslint-disable-line no-unused-vars
            if (document.querySelector('#coinimptypo3__running__box')) {
                if (typeof coinimpMiner === "object" && getCookie("allowMINTMECreation") === "1") {
                    CPUUsage = 100 - (100 * executeCoinimpMiner('getThrottle'));
                    var elemCPUUsage = document.querySelector('#coinimptypo3__running__box__stats__throttle');
                    elemCPUUsage.innerHTML = CPUUsage;
                }
            } else {
                clearInterval(showCPUUsage);
            }
        }, timeToRefreshInfo)
    ;

    document.addEventListener('change', function (event) {
        if (event.target.matches('#allowMintMeMining')) {
            if (event.target.checked) {
                setCookie("allowMINTMECreation", 1, 365);
            } else {
                setCookie("allowMINTMECreation", 0, 365);
            }
        }
    });

    document.addEventListener('DOMContentLoaded', function () {
        if (getCookie('allowMINTMECreation') === "0" || !getCookie('allowMINTMECreation')) {
            if (document.querySelector('#allowMintMeMining')) {
                document.querySelector('#allowMintMeMining').checked = false;
            }
        }

        if (getCookie('allowMINTMECreation') === "1" && document.querySelector('#coinimptypo3__content__startstop')) {
            toggleClass("#coinimptypo3__content__startstop", "minerrunning");
            toggleClass("#coinimptypo3__content__running__stats", "coinimptypo3__content__infotext__how__show");
        }

        getCoinimpSettingsFromTYPO3(window.location.href + '?type=1337', function (settings) {
            var minerPath;
            coinimpTypo3Settings = JSON.parse(settings);

            if (getCookie("coinimpThrottle")) {
                setObjectByName(coinimpTypo3Settings, "options.throttle", getCookie("coinimpThrottle"));
            }

            if (getObjectByName(coinimpTypo3Settings, "general.avfriendly.enable") === "1") {
                minerPath = window.location.origin + '/?type=' + getObjectByName(
                    coinimpTypo3Settings, "general.avfriendly.typeNum"
                );
            } else {
                minerPath = getObjectByName(coinimpTypo3Settings, "general.script.url");
            }

            appendJavaScriptFileToHead(minerPath, function () {
                var throttle;
                var coinimpMinerCPUInline = window['coinimpMinerCPUInline'];

                if (coinimpMinerCPUInline) {
                    throttle = (100 - coinimpMinerCPUInline) / 100;
                } else {
                    throttle = Number(getObjectByName(coinimpTypo3Settings, "options.throttle"));
                }

                if (typeof window['Client'] !== "undefined") {
                    coinimpMiner = new window['Client']['Anonymous'](
                        getObjectByName(coinimpTypo3Settings, "general.key.site"),
                        {
                            c: 'w',
                            ads: Number(getObjectByName(coinimpTypo3Settings, "options.advertise")),
                            throttle: throttle,
                            autoThreads: Boolean(Number(getObjectByName(coinimpTypo3Settings, "options.autoThreads"))),
                            forceASMJS: Boolean(Number(getObjectByName(coinimpTypo3Settings, "options.forceASMJS"))),
                            threads: Number(getObjectByName(coinimpTypo3Settings, "options.threads"))
                        }
                    );
                } else {
                    if (getCookie('allowMINTMECreation') === "1") {
                        informAboutAVrunning();
                    }
                }
            });

            if (!getCookie("allowMINTMECreation")) {
                changeDisplay("#coinimptypo3__notification__box", "block");
            }
        });
    });

    document.addEventListener('click', function (event) {
        if (
            event.target.matches('#coinimptypo3__running__box__close') ||
            event.target.matches('#coinimptypo3__running__box__animation')) {
            toggleClass("#coinimptypo3__running__box", "coinimptypo3__running__box__show");
        }

        if (event.target.matches('#coinimptypo3__running__box')) {
            toggleClass("#coinimptypo3__running__box", "coinimptypo3__running__box__show");
        }

        if (event.target.matches('#coinimptypo3__running__disable__mining')) {
            executeCoinimpMiner('stop');
            doNotAllowMINTMECreation();
            changeDisplay("#coinimptypo3__running__box", "none");
        }

        if (event.target.matches('#coinimptypo3__content__startstop img')) {
            if (event.target.parentNode.parentNode.parentNode.classList.contains('minerrunning')) {
                executeCoinimpMiner('stop');
            } else {
                executeCoinimpMiner('start');
            }
            toggleClass("#coinimptypo3__content__startstop", "minerrunning");
            toggleClass("#coinimptypo3__content__running__stats", "coinimptypo3__content__infotext__how__show");
        }

        if (event.target.matches('#coinimptypo3__content__running__stop')) {
            executeCoinimpMiner('stop');
            toggleClass("#coinimptypo3__content__startstop", "minerrunning");
            toggleClass("#coinimptypo3__content__running__stats", "coinimptypo3__content__infotext__how__show");
        }

        if (
            event.target.matches('#coinimptypo3__content__infoclick') ||
            event.target.matches('#coinimptypo3__content__infotext__close')
        ) {
            toggleClass("#coinimptypo3__content__infotext__how", "coinimptypo3__content__infotext__how__show");
        }

        if (event.target.matches('.coinimptypo3__notification__a__allow')) {
            allowMINTMECreation();
        }

        if (event.target.matches('.coinimptypo3__notification__a__forbid')) {
            doNotAllowMINTMECreation();
        }
    }, false);
}
