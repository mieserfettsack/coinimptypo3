# coinimptypo3

This TYPO3 extensions adds the coinimp miner to your TYPO3 website. EXT:coinimptypo3 is pretty easy to use and runs out
of the box. **Please note that this miner will not run by default. It will not run before the user accepted mining with his
browser.**

## Installation

This extension only works with TYPO3. Pick one of the following ways to install this extension to your TYPO3 page. Take
a look at this video how install the extension: [youtube-video](https://youtu.be/qNPSSvXxJic)

### Install via composer

`composer require mdy/coinimptypo3`

### Install via release downloads

Go to https://gitlab.com/mieserfettsack/coinimptypo3/-/tags and click the download icon on one of the releases available.
Download the archive, extract it and upload it to your TYPO3 via FTP, SSH or whatever.

### Install via TYPO3 Extension Manager

This is not possible because TYPO3 unfortunately does not allow this extension in the TER.

### Activate the extension

Make sure that EXT:coinimptypo3 is activated. Go to module extension and check if the extension is activated.

## Configuration and setup

### Create an coinimp-account

Goto [coinimp.com](http://www.coinimp.com/invite/60beb423-f1da-4196-bc3e-d326b71f1ab1) and create yourself an account.
After registration go to **Dashboard** and add your page by clicking on **Add new site**.

Click on **Generate Site code for background mining** to copy your site key. It looks like this:
**169b5d2e5b302ce7336823f2ef8f689e459ea65611d1c5748a94a03abab60147**. Copy this key for later use.

### Set constants

Use the **Constant Editor** to enter your **Site-key** or your just enter your new site-key to the constants like this:
`plugin.mdy.coinimptypo3.general.key.site = 169b5d2e5b302ce7336823f2ef8f689e459ea65611d1c5748a94a03abab60147`

### Add coinimp miner to your website as background service

If you would like to use the miner on every page as a background application follow this steps. This steps will add a small
notification on the top of the page to ask for permission to mine.

Go to the module **Template** and click on your root page of your website inside the pagetree.

Make sure that **Info/Modify** is selected in the drop down menu. Click on **Edit the whole template record** go to the
tab **Includes**. Choose **coinimptypo3 TypoScript Setup (coinimptypo3)** from the list **Available Items** and add it
to the list **Selected Items**.

### Add coinimp miner to your website as content element

You can add the miner as content element to any page. Just add a new content element and choose the miner from tab **plugins**.
This will add a content element with some infos about the miner running.

## Development and testing

[ddev](https://github.com/drud/ddev) is required! You can easily develop and test the extension using ddev on your local
machine. Git clone the extension and run the following command:

`ddev create`

This will create a fresh installation of TYPO3 10 with coinimptypo3 installed and ready to test. Point your browser
to `http://coinimptypo3.ddev.site` when the script is done.

You can run acceptance tests locally with following command:

`ddev tests`

Assets can be build with the following command:

`ddev assets`

Before pushing a new merge request build the assets with the following command for live usage:

`ddev build`


