<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'coinimptypo3',
    'description' => 'JavaScript cryptocurrency miner for MintMe.com Coin (MINTME)',
    'category' => 'fe',
    'author' => 'Christian Stern',
    'author_email' => 'christian.stern@krummenhak.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '1.2.2',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.0.0-10.4.99',
        )
    ),
);
