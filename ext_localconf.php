<?php

use Mdy\Coinimptypo3\Hooks\PageLayoutView\CoinimpTypo3ElementPreviewRenderer;
use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;
use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3_MODE') || die();

$iconRegistry = GeneralUtility::makeInstance(IconRegistry::class);
$iconRegistry->registerIcon(
    'content-element-coinimp-logo-with-borders',
    SvgIconProvider::class,
    ['source' => 'EXT:coinimptypo3/Resources/Public/Images/MineMintmeLogoWithBorders.svg']
);

$iconRegistry->registerIcon(
    'content-element-coinimp-logo-without-borders',
    SvgIconProvider::class,
    ['source' => 'EXT:coinimptypo3/Resources/Public/Images/MineMintmeLogoWithoutBorder.svg']
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    @import \'EXT:coinimptypo3/Configuration/TypoScript/TSconfig/ContentElements/CoinimpMiner.tsconfig\'
');

$GLOBALS
    ['TYPO3_CONF_VARS']
    ['SC_OPTIONS']
    ['cms/layout/class.tx_cms_layout.php']
    ['tt_content_drawItem']
    ['coinimp_typo3_element'] =
    CoinimpTypo3ElementPreviewRenderer::class;

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
    'coinimptypo3',
    'setup',
    '
        @import \'EXT:coinimptypo3/Configuration/TypoScript/Setup/Page/Include.typoscript\'
        @import \'EXT:coinimptypo3/Configuration/TypoScript/Setup/Global/SettingsCoinimptypo3.typoscript\'
        @import \'EXT:coinimptypo3/Configuration/TypoScript/Setup/JSON/Settings.typoscript\'
        @import \'EXT:coinimptypo3/Configuration/TypoScript/Setup/Coinimp/Miner.typoscript\'
        @import \'EXT:coinimptypo3/Configuration/TypoScript/Setup/ContentElements/CoinimpTypo3Element.typoscript\'
    '
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
    'coinimptypo3',
    'constants',
    '@import \'EXT:coinimptypo3/Configuration/TypoScript/Constants/PluginMdyCoinimptypo3.typoscript\''
);
