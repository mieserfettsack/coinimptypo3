stages:
  - lint
  - build
  - test

build:composer:
  stage: build
  image: composer:latest
  before_script:
    - composer config -g cache-dir "$(pwd)/.composer-cache"
    - mkdir -p Extensions/coinimptypo3
    - cp -R composer.json ext_* Resources Classes Configuration Tests Extensions/coinimptypo3
    - sed -i "s/dev-master/dev-master#$CI_COMMIT_SHA/g" /builds/mieserfettsack/coinimptypo3/Tests/CodeceptJS/Vendor/composer-ddev.json
  script:
    - COMPOSER=Tests/CodeceptJS/Vendor/composer-ddev.json composer install --ignore-platform-reqs
      --optimize-autoloader --no-ansi --no-interaction --no-progress
  cache:
    paths:
      - .composer-cache/
  artifacts:
    expire_in: 1h
    paths:
      - vendor
      - public
      - Extensions

build:yarn:
  stage: build
  image: node:latest
  cache:
    paths:
      - node_modules/
      - .yarn
  artifacts:
    expire_in: 1 hour
    paths:
      - node_modules
      - Tests
  before_script:
    - yarn config set cache-folder .yarn
  script:
    - yarn install --ignore-platform

test:frontend-acceptance:
  image: registry.gitlab.com/mieserfettsack/coinimptypo3/lamp_typo3_codecept:debian10slim
  stage: test
  dependencies:
    - build:composer
    - build:yarn
  before_script:
    - echo "ServerName localhost" >> /etc/apache2/apache2.conf
    - echo "127.0.0.1 coinimptypo3.ddev.site" >> /etc/hosts
    - echo "127.0.0.1 db" >> /etc/hosts
    - sed -i "s/DocumentRoot \/var\/www\/html/DocumentRoot \/builds\/mieserfettsack\/coinimptypo3\/public/g"
      /etc/apache2/sites-available/000-default.conf
    - sed -i "s/<Directory \/var\/www\/>/<Directory \/builds\/mieserfettsack\/coinimptypo3\/public\/>/g"
      /etc/apache2/apache2.conf
    - sed -i "s/Options Indexes FollowSymLinks/Options +FollowSymLinks/g" /etc/apache2/apache2.conf
    - sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
    - /etc/init.d/apache2 restart
    - /etc/init.d/mysql restart
    - mysql -e "DROP DATABASE IF EXISTS db"
    - mysql -e "CREATE DATABASE db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci"
    - mysql -e "GRANT ALL PRIVILEGES ON db.* TO db@localhost IDENTIFIED BY 'db'"
    - mkdir -p /builds/mieserfettsack/coinimptypo3/config/sites/coinimptypo3/
    - cp /builds/mieserfettsack/coinimptypo3/Tests/CodeceptJS/Vendor/TYPO3/Configuration/SiteConfiguration.yaml
      /builds/mieserfettsack/coinimptypo3/config/sites/coinimptypo3/config.yaml
    - vendor/bin/typo3cms install:setup -f -n --web-server-config apache --database-driver mysqli
      --database-user-name db --database-user-password db --database-host-name db --database-name db
      --database-port 3306 --admin-user-name admin --admin-password adminadmin --site-name coinimptypo3
      --site-setup-type no --use-existing-database 1 --site-base-url /
    - vendor/bin/typo3cms install:generatepackagestates
    - vendor/bin/typo3cms database:updateschema "*"
    - vendor/bin/typo3cms impexp:import --updateRecords --ignorePid
      /builds/mieserfettsack/coinimptypo3/Tests/CodeceptJS/Vendor/TYPO3/Content/data.xml 0
    - vendor/bin/typo3cms configuration:set EXTCONF/lang/availableLanguages '["de"]' --json
    - vendor/bin/typo3cms language:update
    - vendor/bin/typo3cms cache:flush
    - chown -R www-data:www-data /builds/mieserfettsack/coinimptypo3/
    - chmod -R 777 /builds/mieserfettsack/coinimptypo3/
  script:
    - /bin/su -s /bin/bash -c 'npx codeceptjs run' www-data

lint:php:
  image: "php:7.2-alpine"
  stage: lint
  only:
    changes:
      - "**/*.php"
  before_script:
    - apk --no-cache add parallel
  script:
    - find . -name \*.php | parallel --gnu php -d display_errors=stderr -l {} > /dev/null \;

lint:composer:
  image: composer:latest
  stage: lint
  only:
    changes:
      - "composer.json"
  script:
    - find . -name composer\.json -exec composer validate --no-check-publish --no-check-all --no-interaction {} \;

lint:javascript:
  image: node:latest
  stage: lint
  cache:
    paths:
      - node_modules/
      - package-lock.json
  only:
    changes:
      - "**/*.js"
  script:
    - npm install -g eslint
    - eslint Resources/Private/Assets/JavaScript/**
    - eslint Resources/Public/JavaScript/**

lint:css:
  image: registry.gitlab.com/imagedb/csslint:1.0.5
  stage: lint
  only:
    changes:
      - "**/*.css"
  script:
    - csslint Resources/Public/CSS/**

lint:TypoScript:
  stage: lint
  image: composer:latest
  before_script:
    - composer config -g cache-dir "$(pwd)/.composer-cache"
    - composer req helmich/typo3-typoscript-lint --ignore-platform-reqs --optimize-autoloader --no-ansi --no-interaction --no-progress
  only:
    changes:
      - "**/*.typoscript"
  cache:
    paths:
      - .composer-cache/
  script:
    - ./vendor/bin/typoscript-lint -c TypoScriptLint.yaml
