<?php

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

(function ($tablename = 'tt_content', $contentType = 'coinimp_typo3_element') {
    ArrayUtility::mergeRecursiveWithOverrule(
        $GLOBALS['TCA'][$tablename], [
        'ctrl' => [
            'typeicon_classes' => [
                $contentType => 'content-element-coinimp-logo-with-borders',
            ],
        ],
        'types' => [
            $contentType => [
                'showitem' => implode(
                    ',', [
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general',
                    '--palette--;;general',
                    'header',
                    'pi_flexform',
                    '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;;frames,
                    --palette--;;appearanceLinks',
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,',
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                      --palette--;;hidden,
                      --palette--;;access'
                ]
                ),
            ],
        ],
        'columns' => [
            'pi_flexform' => [
                'config' => [
                    'ds' => [
                        '*,' . $contentType => 'FILE:EXT:coinimptypo3/Configuration/FlexForms/ContentElement/CoinimpTypo3Element.xml',
                    ],
                ],
            ],
        ],
    ]
    );

    ExtensionManagementUtility::addTcaSelectItem(
        $tablename,
        'CType',
        [
            'LLL:EXT:coinimptypo3/Resources/Private/Language/locallang.xlf:plugin.mdy.coinimptypo3.content_element.coinimp_typo3_element.title',
            $contentType,
            'content-element-coinimp-logo-with-borders',
        ]
    );
})();
