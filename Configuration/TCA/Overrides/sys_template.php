<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3_MODE') or die();

ExtensionManagementUtility::addStaticFile('coinimptypo3', 'Configuration/TypoScript', 'coinimptypo3 TypoScript Setup');
